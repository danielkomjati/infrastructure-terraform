resource "hcloud_server" "server" {
  name         = "${var.env}-${var.type}-${var.region}"
  server_type  = var.size
  image        = var.image_type
  location     = var.region

  network {
    network_id = var.network_id
    ip         = var.ip
    alias_ips  = [for ip in var.alias_ips : ip]
  }

  depends_on   = [var.dependency]
  # **Note**: the depends_on is important when directly attaching the
  # server to a network. Otherwise Terraform will attempt to create
  # server and sub-network in parallel. This may result in the server
  # creation failing randomly.
}
