variable "env" {
  type        = string
  description = "Name of the current environment"
}

variable "type" {
  type        = string
  description = "Type of the server"
}

variable "region" {
  type        = string
  description = "Region where the server is"
}

variable "size" {
  type        = string
  description = "Size of the server (type in Hetzner console)"
}

variable "image_type" {
  type        = string
  description = "Server OS image"
}

variable "network_id" {
  type        = string
  description = "Id of the network which the server is attached to"
}

variable "ip" {
  type        = string
  description = "Specify the IP address the server should get in the network"
}

variable "alias_ips" {
  type = list(string)
  description = "Alias IPs the server should have in the network"
}

variable "dependency" {
  type        = any
  description = "Item that the server depends on"
}