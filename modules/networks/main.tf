resource "hcloud_network" "main_network" {
  name     = "${var.env}-main-network-${var.region}"
  ip_range = var.main_network_ip_range
}

resource "hcloud_network_subnet" "subnets" {
  for_each     = var.subnets
  network_id   = hcloud_network.main_network.id
  type         = each.value["type"]
  network_zone = each.value["zone"]
  ip_range     = each.value["ip_range"]
  depends_on   = [hcloud_network.main_network]
}