variable "env" {
  type        = string
  description = "Current env's name"
}

variable "region" {
  type        = string
  description = "Region of the networks"
}

variable "main_network_ip_range" {
  type        = string
  description = "IP range of the main network"
}

variable "subnets" {
  type = list(object({
    ip_range     = string
    availability = string
    zone         = string
    type         = string
  }))
  description = "List of subnets and their details in a map object"
}