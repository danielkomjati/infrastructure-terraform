hcloud_token = "tokenPlaceholder"

environments = {

  qa = {
    region   = "eu-central"
    networks = {
      main_network_ip_range = "10.1.0.0/22"
      subnets               = [
        {
          ip_range     = "10.1.0.0/24"
          availability = "public"
          zone         = "eu-central"
          type         = "cloud"
        },
        {
          ip_range     = "10.1.1.0/24"
          availability = "private"
          zone         = "eu-central"
          type         = "cloud"
        },
        {
          ip_range     = "10.1.3.0/24"
          availability = "restricted"
          zone         = "eu-central"
          type         = "cloud"
        }
      ]
    }
    bastion      = {
      bastion_ip = "?"
      region     = "eu-central"
      size       = "cx21"
      type       = "bastion"
    }
  }

  staging = {}

  prod = {}
}