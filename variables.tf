# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {
  sensitive   = true # Requires terraform >= 0.14
  description = "The token value that the provider needs"
}

variable "env" {
  type        = string
  description = "Environment name"
}

variable "environments" {
  type        = map(any)
  description = "Network details per environment"
}