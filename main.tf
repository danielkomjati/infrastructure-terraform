provider "hcloud" {
  token = var.hcloud_token
}

module "main_networks" {
  for_each              = var.environments
  source                = "modules/networks"
  env                   = each.key
  region                = each.value["region"]
  main_network_ip_range = each.value["networks"]["main_network_ip_range"]
  subnets               = [
    for subnet in each.value["networks"]["subnets"] : {
      ip_range     = subnet["ip_range"]
      availability = subnet["availability"]
      zone         = subnet["zone"]
      type         = subnet["type"]
    }
  ]
}

module "bastion" {
  for_each   = var.environments
  source     = "modules/servers"
  # Main vars:
  region     = each.value["bastion"]["region"]
  size       = each.value["bastion"]["size"]
  type       = each.value["bastion"]["type"]
  env        = each.key
  network_id = module.main_networks[1]["id"]
  # Network block vars:
  image_type = "ubuntu-20.04"
  ip         = ""
  alias_ips  = []
  #
  dependency = module.main_networks
}